/*
Title: Smart TV
Author: Caitlin
Date: 7/06/18
Purpose:  Capture the following type of information for the smart TVs that are imported.
*/

//the name of the 'SmartTV' class
class SmartTV{
    get make(){
        return this._make; //"default" getter and setter for the "make" property
    }
    set make(value){
        this._make = value;
    }
    get model(){
        return this._model;//"default" getter and setter for the "model" property
    }
    set model(value){
        this._model = value;
    }
    get size(){
        return this._size;//"default" getter and setter for the "size" property
    }
    set size(value){
        this._size = value;
    }
    get price(){
        return this._price;//"default" getter and setter for the "price" property
    }
    set price(value){
        this._price = value;
    }
    //Constructor for the "SmartTV" class
    constructor(make, model, size, price, ){
        this._make = make;
        this._model = model;
        this._size = size;
        this._price = price;
        
    }//Method for the Price when using a credit card
    ccPrice(){
        let overall = (this._price/100) * levy //the price divided by 100 + the levy to get the cost that needs to be added on
        return this._price + overall // returns the price with the credit card price added on
    }
}
//Main Program
//Creat a new object "one" using the "SmartTV" class as a template

let one = new SmartTV

//Pass arguments to the class by prompting the user for input
one.make = prompt("Please enter the make of the TV:")
one.model = prompt("Please enter the model of the TV:")
one.size = prompt("Please enter the size of the TV(inches):")
one.price = Number(prompt("Please enter the price of the TV:"))

//All information displayed in browser
console.log(`Smart Buy TV's ltd: Smart TV Details`)//heading
console.log(`------------------------------------`)
console.log(`TV Make                : ${one.make}`)//the tv make for "one"
console.log(`TV Model               : ${one.model}`)//the tv model for "one"
console.log(`Screen SizeI(inches)   : ${one.size}`)//the tv size for "one"
console.log(`Price                  : $${one.price}`)//the tv price for "one"
levy = prompt("Please enter the credit card Levy(%):")//prompts the user for levy
console.log(``)
console.log(`Price using Credit Card: $${one.ccPrice().toFixed(2)}`)//uses the "ccPrice" method the display the price when using the credit card. The to.Fixed makes it have only two decimal places.